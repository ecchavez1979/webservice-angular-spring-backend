package com.qualityadvisors.empresa;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


@SpringBootTest
@RunWith(SpringRunner.class)
@DataJpaTest
public class ControladorTest {

	 private MockMvc mockMvc;

	    @Mock
	    private PersonaServiceImp personaService;

	    @InjectMocks
	    private Controlador controlador;

	    @Before
	    public void setUp() throws Exception {
	        mockMvc = MockMvcBuilders.standaloneSetup(personaService)
	                .build();
	    }

	    @Test
	    public void testHelloWorld() throws Exception {


	        List<Persona> listaPersona = new ArrayList<Persona>();
			when(personaService.listar()).thenReturn(listaPersona);

	        mockMvc.perform(get("/personas"))
	                .andExpect(status().isOk());

	        verify(personaService).listar();
	    }

	    @Test
	    public void testHelloWorldJson() throws Exception {
	        mockMvc.perform(get("/hello/json")
	                .accept(MediaType.APPLICATION_JSON))
	                .andExpect(status().isOk())
	                .andExpect(jsonPath("$.title", Matchers.is("Greetings")))
	                .andExpect(jsonPath("$.value", Matchers.is("Hello World")))
	                .andExpect(jsonPath("$.*", Matchers.hasSize(2)));
	    }

	    @Test
	    public void testPost() throws Exception {
	        String json = "{\n" +
	                "  \"title\": \"Greetings\",\n" +
	                "  \"value\": \"Hello World\"\n" +
	                "}";
	        mockMvc.perform(post("/hello/post")
	                .contentType(MediaType.APPLICATION_JSON)
	                .content(json))
	                .andExpect(status().isOk())
	                .andExpect(jsonPath("$.title", Matchers.is("Greetings")))
	                .andExpect(jsonPath("$.value", Matchers.is("Hello World")))
	                .andExpect(jsonPath("$.*", Matchers.hasSize(2)));
	    }

}

