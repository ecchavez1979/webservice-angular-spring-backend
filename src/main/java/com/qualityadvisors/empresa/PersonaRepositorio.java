
package com.qualityadvisors.empresa;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;


public interface PersonaRepositorio extends CrudRepository<Persona, Integer>{
    List<Persona>findAll();
    Persona findOne(int id);
    @SuppressWarnings("unchecked")
	Persona save(Persona p);
    void delete(Persona p);
}
