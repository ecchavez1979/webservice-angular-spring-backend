package com.qualityadvisors.empresa;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@DataJpaTest
public class Ejemplo01ApplicationTests {

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private  PersonaService personaService;
	
	@Test
	public void testSaveTicket(){
		Persona ticket = getPersona();
		Persona savedInDb = entityManager.persist(ticket);
		Persona getFromDb = personaService.listarId(savedInDb.getId());
		
		assertThat(getFromDb).isEqualTo(savedInDb);
	}	
	
	private Persona getPersona() {
		Persona persona = new Persona();
		persona.setName("Michaell");
		persona.setApellidos("Jordan");
		return persona;	
	}

}
